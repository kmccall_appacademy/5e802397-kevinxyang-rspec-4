class Book
  # TODO: your code goes here!

  attr_accessor :title

  # def initialize(title)
  #   @title = title
  # end

  def title=(title)
    word_arr = title.split(' ')
    exceptions = %w(the a an and or in of)
    new_title = [word_arr[0].capitalize]

    word_arr[1...word_arr.length].each do |word|
      if exceptions.include?(word)
        new_title << word
      else
        new_title << word.capitalize
      end
    end

    @title = new_title.join(' ')
  end

end

## Finish this and the other RSPEC 4 exercises by April 28 - 5:30pm

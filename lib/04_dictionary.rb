class Dictionary

  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(user_entry)
    @entries[user_entry] = nil if user_entry.is_a?(String)
    @entries.merge!(user_entry) if user_entry.is_a?(Hash)
    @entries.sort_by { |a| a }
  end

  def keywords
    @entries.keys.sort { |x, y| x <=> y }
  end

  def include?(keyword)
    @entries.has_key?(keyword)
  end

  def find(str)
    @entries.select { |key, value| key[0...str.length] == str }

  end

  def printable
    printing = @entries.sort.map do |keyword, definition|
      "[#{keyword}] \"#{definition}\""
    end

    printing.join("\n")
  end

end

class Temperature

  def initialize(option)
    if option[:f]
      @fahrenheit = option[:f]
    elsif option[:c]
      @celsius = option[:c]
    end
  end

  def in_fahrenheit
    if @fahrenheit
      return @fahrenheit
    elsif @celsius
      (@celsius * 9 / 5.0) + 32
    end
  end

  def in_celsius
    if @fahrenheit
      return (@fahrenheit - 32) * (5 / 9.0)
    elsif @celsius
      @celsius
    end
  end

  def self.from_fahrenheit(temp)
    Temperature.new(f: temp)
  end

  def self.from_celsius(temp)
    Temperature.new(c: temp)
  end

end

  class Celsius < Temperature
    def initialize(temp)
      @celsius = temp
    end
  end

  class Fahrenheit < Temperature
    def initialize(temp)
      @fahrenheit = temp
    end
  end

class Friend
  # TODO: your code goes here!

  def greeting(who = nil)
    return "Hello!" unless who
    "Hello, #{who}!"
  end

end

## Finish this and the other RSPEC 4 exercises by April 28 - 5:30pm

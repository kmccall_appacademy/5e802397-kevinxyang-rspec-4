class Timer

  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def padder(number)
    if number < 10
      "0#{number}"
    elsif number < 99
      "#{number}"
    end
  end

  def hours
    hours = (seconds / (60 * 60))
  end

  def minutes
    minutes = (seconds % (60 * 60)) / 60
  end

  def seconds_left
    seconds_left = seconds % 60
  end

  def time_string
    "#{padder(hours)}:#{padder(minutes)}:#{padder(seconds_left)}"
  end
end
